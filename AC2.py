from flask import Flask, jsonify

app = Flask(__name__)

# Dados do produto
produto = {
    'id': 1,
    'nome': 'Bola de Futebol',
    'preco': 39.99,
    'descricao': 'Bola de futebol tamanho oficial',
    'categoria': 'Esportes'
}

# Rota para obter as informações do produto


@app.route('/produto', methods=['GET'])
def obter_produto():
    return jsonify(produto), 200


if __name__ == '__main__':
    app.run(debug=True)
